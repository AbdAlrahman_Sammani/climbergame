﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ShapesEquations 
{
   public static float GetCirclePoint(float radius,Vector2 origin,float y)
    {
        float x;
        x = Mathf.Sqrt(Mathf.Pow(radius, 2) + Mathf.Pow(y + origin.y, 2)) + origin.x;
        return x;
    }
    public static float CircleRound(float radius)
    {
        return Mathf.PI * radius;
    }
}
