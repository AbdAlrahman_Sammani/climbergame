﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public enum GameState { MainMenu,InGameNotStart,InGamePlay}
public class GameManager : MonoBehaviour {

    public GameObject springInUse;
    public GameObject hand;
    public GameObject playerMainGameObject;
    public Vector3 targetToMoveTo;
    public LineRenderer connectedLine;
    public static GameManager Instance;
    public Rigidbody[] allRigidBodys;
    public GameEventsArgs gameEventsArgs;



    private void OnEnable()
    {
        GameEvents.OnGamePlayStartEvent += TogglePLayerKinematicState;

    }
    private void OnDisable()
    {
        GameEvents.OnGamePlayStartEvent -= TogglePLayerKinematicState;

    }

    void Start()
    {
        Instance = this;
       
    }

    // Update is called once per frame
    void Update()
    {
        CalcLine();
    }

    public void CalcLine()
    {

       Vector3[] positions = { springInUse.transform.position, hand.transform.position};
        connectedLine.SetPositions(positions);
        connectedLine.SetColors(Color.white, Color.white);
    }
    public void test()
    {

        Debug.Log("SSSS");
    }

    public void TogglePLayerKinematicState(GameEventsArgs args)
    {
        allRigidBodys = playerMainGameObject.GetComponentsInChildren<Rigidbody>();
        foreach (var item in allRigidBodys)
        {
            item.isKinematic = args.kinematicState;
        }
    }
    public void RestartGame()
    {
        SceneManager.LoadScene(0);

    }
}
