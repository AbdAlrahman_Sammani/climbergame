﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickerActivator : MonoBehaviour
{
    public GameObject[] stickers;
    public GameObject connectedBody;
    public List<GameObject> activeStickers;
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(3f);
        Invoke("GetStickers", 3);
        Debug.Log("'GetStickers");
        //  GetStickers();
    }

    // Update is called once per frame
    void Update()
    {
      
        transform.position = connectedBody.transform.position;
        CheckStickerPosition();

    }
    public void GetStickers()
    {
        stickers = GameObject.FindGameObjectsWithTag("Points");
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Points"))
        {
            other.GetComponent<MeshRenderer>().material.color = Color.red;
            activeStickers.Add(other.gameObject);
        //    Debug.Log("InPoint");
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Points"))
        {
            other.GetComponent<MeshRenderer>().material.color = Color.white;
         //   Debug.Log("InPoint");
            activeStickers.Remove(other.gameObject);
        }
    }
    public void CheckStickerPosition()
    {
        float posVal = 0;
        if (activeStickers.Count > 0)
        {
            foreach (var item in activeStickers)
            {
                posVal = item.transform.position.x + posVal;
            }
            if (Mathf.Abs(posVal) - Mathf.Abs(connectedBody.transform.position.x) < 0)
            {
                CameraController.Instance.SwitchCameraRight();

            }
            else
            {
                CameraController.Instance.SwitchCameraLeft();
            }
          //  Debug.Log("Body" + connectedBody.transform.position.x);
           // Debug.Log("Val" + posVal);
        }
    }
}
