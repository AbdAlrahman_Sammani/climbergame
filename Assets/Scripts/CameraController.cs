﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject cameraObjectToFollow;
    public Vector3 offset;
    public Vector3 rotate;
    public GameObject connectedBody;
    private Vector3 tempOffest, tempRotate;
    public static CameraController Instance;
    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        SetValuesInLerp();
        CameraRotationHandler();

    }
    private void LateUpdate()
    {
        //Debug.Log(connectedBody.transform.localPosition);
      //  if (connectedBody.transform.localPosition != Vector3.zero)
            Camera.main.transform.position = new Vector3(cameraObjectToFollow.transform.position.x + desierdOffset, cameraObjectToFollow.transform.position.y - offset.y, Camera.main.transform.position.z);
    }


    public void CameraRotationHandler()
    {
        if (connectedBody.transform.localPosition == Vector3.zero)
        {
            Camera.main.transform.rotation = Quaternion.Euler(0, 0, 0);
            Camera.main.transform.position = new Vector3(0, 0, Camera.main.transform.position.z);
        }

        Camera.main.transform.rotation = Quaternion.Euler(rotate.x, desiredRot, 0);
    }
    public void SwitchCameraRight()
    {
        offset.x = -35;
        rotate.y = 20;

    }
    public void SwitchCameraLeft()
    {
        offset.x = 35;
        rotate.y = -20;
    }
    public float desiredRot, desierdOffset;
    public void SetValuesInLerp()
    {
        desiredRot = Mathf.Lerp(desiredRot, rotate.y, Time.deltaTime*2);
        desierdOffset = Mathf.Lerp(desierdOffset, offset.x, Time.deltaTime*2);
    }
}
