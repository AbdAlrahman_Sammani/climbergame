﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum PowerupType {Translate}
public class PowerupsHandler : MonoBehaviour
{
    #region Translate Powerup
    public bool usePowerUp;
    public void ApplyForce(Vector3 direction, Rigidbody rb)
    {
        if (usePowerUp)
            rb.AddForce(direction * 10, ForceMode.Impulse);
    }
    public IEnumerator UsePowerUp(float time)
    {
        usePowerUp = true;
        yield return new WaitForSeconds(time);
        usePowerUp = false;
    }
    #endregion
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

}
