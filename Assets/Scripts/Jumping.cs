﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Jumping : MonoBehaviour
{
    public GameObject leftHandSpring, rightHandSpring, leftLegSpring, rightLegSpring;
    public GameObject body;
    private GameObject springToUse;
    public GameObject bodyContainer;
    public GameObject[] springs;


    public bool isSpringInUse;

    public float initSpringValue,
                 initSpringValueMultiplier;
    public float maxSpringValue;
    public float increasingSpringStep,
                 increasingSpringStepMultiplier;
    public float connectedMassScale,
                 springMassScale;
    public float velocityVal;
    public float maxVelocity;

    private Vector3 desiredPositionToHoldSpring;
    private float distance;


    // Start is called before the first frame update
    void Start()
    {
        springs = new GameObject[4];
        springs[0] = leftHandSpring;
        springs[1] = rightHandSpring;
        springs[2] = leftLegSpring;
        springs[3] = rightLegSpring;
        springToUse = leftHandSpring;
        // SetSpringPosition(10);

    }

    // Update is called once per frame
    void Update()
    {
        //body.GetComponent<Rigidbody>().velocity = new Vector3(10, 10, 0);// new Vector3(Mathf.Clamp(body.GetComponent<Rigidbody>().velocity.x, -clampingVelcociyValue, Mathf.Abs(clampingVelcociyValue)), Mathf.Clamp(body.GetComponent<Rigidbody>().velocity.y, -clampingVelcociyValue, Mathf.Abs(clampingVelcociyValue)), body.GetComponent<Rigidbody>().velocity.z);

        InUpdateProcess();
        VelocityHandler();


        if (Input.GetMouseButton(0))
        {
            ActiveSlowMotion();
       
        }
        if (Input.GetMouseButtonUp(0))
        {
            Time.timeScale = 1;
        }
        if (Input.GetMouseButtonDown(1))
        {

        }
    //    Debug.Log("TEST" + Mathf.Abs((int)body.transform.position.x - (int)desiredPositionToHoldSpring.x));

    }
    public void InUpdateProcess()
    {

       // Debug.Log("MaAG"+ Mathf.Abs(body.GetComponent<Rigidbody>().velocity.magnitude));
        // Debug.Log((body.GetComponent<Rigidbody>().velocity.y));
        try
        {
            if (Input.GetMouseButtonDown(0))
            {

                if (body.GetComponent<Rigidbody>().velocity.y < 0)
                {
                    SetSpringPosition(initSpringValue);

                }
                else
                {
                    SetSpringPosition(initSpringValue * initSpringValueMultiplier);
                }

            }
            // Debug.Log(body.GetComponent<Rigidbody>().velocity.magnitude);

            ///To Release Spring
            if (springToUse != null)
            {
                if (springToUse.GetComponent<SpringJoint>().spring < maxSpringValue)
                {
                    if (Mathf.Abs(body.GetComponent<Rigidbody>().velocity.magnitude) < 50)
                        springToUse.GetComponent<SpringJoint>().spring += increasingSpringStep * increasingSpringStepMultiplier;
                    else
                    {
                        springToUse.GetComponent<SpringJoint>().spring += increasingSpringStep;
                    }
                }
                if (springToUse.GetComponent<SpringJoint>().spring >=maxSpringValue)
                {
                    springToUse.GetComponent<SpringJoint>().spring = 0;
                }

                if ((int)body.transform.position.y > (int)desiredPositionToHoldSpring.y )//&& Mathf.Abs((int)body.transform.position.x-(int)desiredPositionToHoldSpring.x)<20)
                {
                    if (body.GetComponent<Rigidbody>().velocity.y > 0 && Mathf.Abs(body.GetComponent<Rigidbody>().velocity.magnitude) < 300)
                    {
                        ReleaseSpring();
                    }
                    else
                    {
                        //springToUse.GetComponent<SpringJoint>().damper += 0.2f;
                    }
                }
                //if (Vector3.Distance(springToUse.transform.position, springToUse.GetComponent<SpringJoint>().connectedBody.transform.position) < 5)
                //{
                //    if (body.GetComponent<Rigidbody>().velocity.y > 0 && Mathf.Abs(body.GetComponent<Rigidbody>().velocity.magnitude) < 100)
                //    {
                //        ReleaseSpring();
                //    }
                //    else
                //    {
                //        springToUse.GetComponent<SpringJoint>().damper += 0.2f;
                //    }
                //}
            }
        }
        catch (System.Exception e)
        {
            Debug.LogError(e.Message);
        }
    }


    /// <summary>
    /// Call This When Need To Hold a Spring
    /// </summary>
    /// <param name="initSpringValue"></param>
    public void SetSpringPosition(float initSpringValue)
    {

        GameEvents.CallOnGameStartEvent(GameManager.Instance.gameEventsArgs);
        desiredPositionToHoldSpring = TabHandler.Instance.posToSent;//GetMouseInputWorldPositon();
        if (TabHandler.Instance.isSticker == false) return;
        //if (desiredPositionToHoldSpring == Vector3.zero)
        //{
        //    desiredPositionToHoldSpring = TapHandler.Instance.pointer.transform.position;
        //}

        ///Choose The closest Spring To Point
        float tempDistance;
        foreach (var item in springs)
        {
            tempDistance = distance;
            distance = Vector2.Distance(item.transform.position, desiredPositionToHoldSpring);
        }

        springToUse = springs[0];//springs[Random.Range(0,3)]; /// change this in advance
        springToUse.gameObject.SetActive(true);
        GameManager.Instance.connectedLine.enabled = true; /// Enable Line


        foreach (var item in springs)
        {
            if (item == springToUse) continue;
            else
            {
                item.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezePositionZ;
            }

        }
        springToUse.GetComponent<Rigidbody>().mass = 1;
        springToUse.GetComponent<SpringJoint>().connectedMassScale = connectedMassScale;
        springToUse.GetComponent<SpringJoint>().massScale = springMassScale;
        springToUse.GetComponent<SpringJoint>().spring = initSpringValue;
        springToUse.transform.position = new Vector3(desiredPositionToHoldSpring.x, desiredPositionToHoldSpring.y, 0);
        springToUse.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY;
        GameManager.Instance.springInUse = springToUse;


    }
    /// <summary>
    /// Call This When Release Spring
    /// </summary>
    public void ReleaseSpring()
    {
        try
        {
            springToUse.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezePositionZ;
            springToUse.GetComponent<SpringJoint>().spring = 1000;
            springToUse.GetComponent<SpringJoint>().damper = 0;
            springToUse.GetComponent<SpringJoint>().connectedMassScale = 1f;
            springToUse.GetComponent<SpringJoint>().massScale = 1f;
            springToUse.gameObject.SetActive(false);
            GameManager.Instance.connectedLine.enabled = false;

            foreach (var item in springs)
            {
                // item.GetComponent<SpringJoint>().spring =500 ;
                //  item.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezePositionZ;
                // item.gameObject.SetActive(false);
            }
            springToUse = null;
        }
        catch (System.Exception ex)
        {
            throw ex;
        }
    }

    public void ActiveSlowMotion()
    {
        Time.timeScale = 0.5f;
    }


    /// <summary>
    /// InUpdate To Limit The Player Velocity
    /// </summary>
    public void VelocityHandler()
    {
        if (body.GetComponent<Rigidbody>().velocity.magnitude > velocityVal)
        {

            Rigidbody rb = body.GetComponent<Rigidbody>();
            //Debug.Log("InVel=" + rb.velocity.magnitude);
            rb.velocity = new Vector3(Mathf.Clamp(rb.velocity.x, -maxVelocity, maxVelocity), Mathf.Clamp(rb.velocity.y, -maxVelocity, maxVelocity), 0);
        }
    }
    public bool usePowerUP;
  
    public void JumpingDefault()
    {
        // Reset Position
        // Reset Kinematic 
       
    }
}
