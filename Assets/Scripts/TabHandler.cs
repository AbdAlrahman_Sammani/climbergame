﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TabHandler : MonoBehaviour
{
    public Vector3 posToSent;
    public static TabHandler Instance;
    private string sticker;
    public bool isSticker;
    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
        sticker = "Points"; 

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
           //     Debug.Log(hit.transform.gameObject.tag);
                if (hit.transform.gameObject.tag == sticker && hit.transform.gameObject.GetComponent<MeshRenderer>().material.color == Color.red)
                {
                    Rigidbody rb;
                    if (rb = hit.transform.GetComponent<Rigidbody>())
                    {
                        {
                            PrintPosName(hit.transform.gameObject);
                            posToSent = hit.transform.gameObject.transform.position;
                            isSticker = true;
                        }
                    }
                }
                else
                {
                    isSticker = false;
                }
               
            }
        }

    }

    private void PrintPosName(GameObject go)
    {
        print(go.name + " is at "+go.transform.position);
    }

}
