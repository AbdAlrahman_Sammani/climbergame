﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public struct GameEventsArgs
{
  public   bool kinematicState;
}
public static class GameEvents 
{

    

    public delegate void OnGamePlayStart(GameEventsArgs args);
    public static event OnGamePlayStart OnGamePlayStartEvent;
    public static void CallOnGameStartEvent(GameEventsArgs args)
    {
        OnGamePlayStartEvent.Invoke(args);
    }


}
