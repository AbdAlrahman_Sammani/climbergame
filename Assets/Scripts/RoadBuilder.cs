﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadBuilder : MonoBehaviour
{
   
    
    public GameObject RoadRock;
    public GameObject Sticker;
   
    private List<Vector3> List = new List<Vector3>();
    // Start is called before the first frame update
    void Start()
    {
        Build(82);
        Stickerdistribut();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    /// <summary>
    /// build the road path
    /// </summary>
    /// <param name="LevelSize"> number of rocks </param>
    void Build(int LevelSize)
    {
        GameObject  newRoadRock = Instantiate(RoadRock, new Vector3(0, -111, 10) , Quaternion.identity);
        Vector3 prevPosition = newRoadRock.transform.position;
        List.Add(prevPosition);
        for (int i = 0; i < LevelSize; i++)
        {
            Vector3 newPosition = new Vector3
               (
            /* x */   prevPosition.x + Random.Range(-newRoadRock.transform.localScale.x * 10, newRoadRock.transform.localScale.x * 10),
            /* y */   prevPosition.y + RoadRock.transform.localScale.y*6,
            /* z */   prevPosition.z 
               );
            
             newRoadRock = Instantiate(newRoadRock, newPosition, Quaternion.identity);
            
            prevPosition = newRoadRock.transform.position;
            List.Add(prevPosition);
        }



    }
    void Stickerdistribut()
    {
        for (int i = 0; i < List.Count; i++)
        {

            GameObject newSticker = Instantiate(Sticker,new Vector3( List[i].x, List[i].y ,List[i].z -6), Quaternion.identity);

        }
        


    }
}
